package com.curso.spring.api.repositorie;

import com.curso.spring.api.models.Categoria;
import com.curso.spring.api.repositories.CategoriaRepository;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class CategoriaRepositoryTest {
    
    @Autowired
    CategoriaRepository CategoriaRepository;

    @Test
    public void save(){
        Categoria categoria = new Categoria();
        categoria.setId(4);
        categoria.setDescripcion("verduras");
        categoria = CategoriaRepository.save(categoria);
        Assert.assertEquals(categoria, CategoriaRepository.findById(4).get());
    }

}
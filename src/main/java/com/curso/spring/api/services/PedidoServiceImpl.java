package com.curso.spring.api.services;

import java.util.List;

import javax.transaction.Transactional;

import com.curso.spring.api.models.Detalle;
import com.curso.spring.api.models.Pedido;
import com.curso.spring.api.repositories.DetalleRepository;
import com.curso.spring.api.repositories.PedidoRepository;

import org.springframework.beans.factory.annotation.Autowired;

public class PedidoServiceImpl implements PedidoService {

    @Autowired
    private PedidoRepository pedidoRepo;

    @Autowired
    private DetalleRepository detalleRepo;

    @Override
    @Transactional
    public Pedido save(Pedido pedido, List<Detalle> detalles) {
        Pedido pedidoGuardado = pedidoRepo.save(pedido);
        for(Detalle tmp: detalles){
            tmp.setPedido(pedidoGuardado);
            detalleRepo.save(tmp);
        }
        return null;
    }

    
}
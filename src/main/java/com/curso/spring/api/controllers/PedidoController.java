package com.curso.spring.api.controllers;

import java.util.List;

import com.curso.spring.api.models.Pedido;
import com.curso.spring.api.repositories.PedidoRepository;

import org.springframework.beans.factory.annotation.Autowired;

public class PedidoController {
    @Autowired
    private PedidoRepository pedidoRepository;

    public List<Pedido> getPedido(){
        List<Pedido> pedido = pedidoRepository.findAll();
        return pedido;
    }
}
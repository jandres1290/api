package com.curso.spring.api.controllers;

import java.util.List;

import com.curso.spring.api.repositories.ProductoRepository;
import com.curso.spring.api.models.Producto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductoController {
    @Autowired
    private ProductoRepository productoRepository;

    @GetMapping("/producto")
    @PreAuthorize("hasRole('ADMIN')")
    public List<Producto> getProductos(){
        List<Producto> producto = productoRepository.findAll();
        return producto;
    }

    @PostMapping("/producto")
    public Producto postProducto(@RequestBody Producto producto){
        return productoRepository.save(producto);
    }
}
package com.curso.spring.api.controllers;

import java.util.List;
import java.util.Optional;

import com.curso.spring.api.models.Categoria;
import com.curso.spring.api.repositories.CategoriaRepository;
import com.curso.spring.api.response.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CategoriaController {
    
    @Autowired
    private CategoriaRepository categoriaRepo;

    @GetMapping("/saludar/{nombre}")
    public String saludar(@PathVariable String nombre){
        return "HOLA" + nombre;
    }

   /* @GetMapping("/categoria/{id}")
    public ResponseEntity<Response<Categoria>> getCategoriaById(@PathVariable int id){
        Categoria categoria = categoriaRepo.findById(id).get();
        Response<Categoria> response = new Response<>();
        response.setResultado(categoria);
        response.getErrores().add("categoria");

        return ResponseEntity.ok(response);
    }*/

    /*@GetMapping("/categoria/{id}")
    public ResponseEntity<Response<Categoria>> getCategoriaById(@PathVariable int id){
        Response<Categoria> response = new Response<>();
        Optional categoria = categoriaRepo.findById(id).get();
        if (!categoria.isPresent()){
            response.getErrores().add("No existe el elemento");
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
        }
        response.setResultado(categoria);
        return ResponseEntity.ok(response);
    }*/

    @GetMapping("/categoria")
    public List<Categoria> getCategoria(){
        List<Categoria> categoria = categoriaRepo.findAll();
        return categoria;
    }

    @PostMapping("/categoria")
    public Categoria postCategoria(@RequestBody Categoria categoria){
        return categoriaRepo.save(categoria);
    }
}